﻿using FundIt.Connections;
using FundIt.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace FundIt
{
    public class HelperFunctions
    {
        //hashing a string
        public static string sha256(string randomString)
        {
            var crypt = new System.Security.Cryptography.SHA256Managed();
            var hash = new System.Text.StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(randomString));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return hash.ToString();
        }

        public static string gen_coin(Users user)
        {
            string hash_code="";
            var query = "Select top 1 hash_code From wallet order by sr_no desc";
            SqlConnection connection = new SqlConnection(DbConnector.connectionString);
            //SqlCommand command = new SqlCommand(query, connection);
            connection.Open();
            using (var selectCommand = connection.CreateCommand())
            {
                selectCommand.CommandText = query;
                //...command parameters setup here if necessary

                using (var reader = selectCommand.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (reader.Read())
                    {
                        //process data here
                         hash_code = (string)reader["hash_code"];
                        //and so on, you get the idea...
                    }
                }
            }
            connection.Close();
            hash_code = hash_code + user.wallet_no + "50";

            hash_code = HelperFunctions.sha256(hash_code);
            
            return hash_code;

            
        }
    }
}