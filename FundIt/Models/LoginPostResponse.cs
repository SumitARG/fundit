﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FundIt.Models
{
    public class LoginPostResponse
    {
        public string message;
        public int is_user_funder;
        public int success;
    }
}