﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FundIt.Models
{
    public class ListOfBusiness
    {
        public string product_name { get; set; }
        public int cost { get; set; }
        public int duration { get; set; } = 5;
        public int request_id { get; set; }
    }
}