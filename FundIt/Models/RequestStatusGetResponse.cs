﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FundIt.Models
{
    public class RequestStatusGetResponse
    {
        public string product_name { get; set; }
        public string activity_status { get; set; }
        public string activity_name { get; set; }
        public string amount_status { get; set; }
    }
}