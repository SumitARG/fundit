﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FundIt.Models
{
    public class Business
    {
        public string email { get; set; }
        public string request_id { get; set; } = "";
        public string product_name { get; set; }
        public string is_going { get; set; }
    }
}