﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FundIt.Models
{
    public class VerifyPostResponse
    {
        public string hash_code { get; set; }
        public int amount { get; set; } = 0;
    }
}