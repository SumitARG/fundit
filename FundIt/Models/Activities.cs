﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FundIt.Models
{
    public class Activities
    {
        public string product_name { get; set; }
        public int request_id { get; set; }
        public string[] activity_name { get; set; }
        public int activity_id { get; set; }
        public int[] duration { get; set; } = { 2, 2, 2, 2, 2 };
        public int cost { get; set; } 
        public int activity_cost { get; set; }
        public bool activity_on_going { get; set; }
        
    }
}