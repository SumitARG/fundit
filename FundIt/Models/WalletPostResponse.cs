﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FundIt.Models
{
    public class WalletPostResponse
    {
        public string wallet_no { get; set; }
        public string hash_code { get; set; }
    }
}