﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FundIt.Models
{
    public class Funder
    {
        public static string email { get; set; }
        public static string request_id { get; set; }
        public static int amount_invested { get; set; }
    }
}