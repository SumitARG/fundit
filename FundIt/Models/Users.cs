﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FundIt.Models
{
    public class Users
    {
        public string name { get; set;}
        public string email { get; set; }
        public string password { get; set; }
        public int is_user_funder { get; set; } = 1;
        public string wallet_no { get; set; } = "";
    }
}