﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FundIt.Models;
using FundIt.ProcessHelpers;
using FundIt.RepositoriesHelpers;

namespace FundIt.Controllers
{
    public class BusinessController : ApiController
    {
        List<ListOfBusiness> businessList = new List<ListOfBusiness>();
        [HttpPost]
        [Route("CreateRequest")]
        public FundingRequestPostResponse checkRequest(Useless useless)
        {
            FundingRequestPostResponse res = new FundingRequestPostResponse();
            var responsebool = BusinessRepository.checkRequestStatus(Temp_Data.temp_email);
            if (responsebool == true)
            {
                res.message = "Eligible For Request Creation";
                res.is_going = 1;
            }

            else
            {
                res.message = "Ongoing Request!! Cannot Create New!!";
                res.is_going = 0;
            }

            return res;
        }

        [HttpPost]
        [Route("CreateActivities")]
        public ActivityPostResponse createRequest(Activities activities)
        {
            ActivityPostResponse res = new ActivityPostResponse();
            var responsebool = BusinessRepository.createRequest(activities);
            if (responsebool != 0)
            {
                ListOfBusiness lb = new ListOfBusiness();
                res.message = "Request Created Successfully";
                lb.cost = activities.cost;
                lb.product_name = activities.product_name;
                lb.request_id = responsebool;
                BusinessRepository.addToBusinessList(lb);
            }

            else
                res.message = "Request Not Successful Try Again";
            return res;
        }

        [HttpGet]
        [Route("getWalletDetails")]
        public WalletPostResponse walletDetails()
        {
            WalletPostResponse res = new WalletPostResponse();
            res = BusinessRepository.getWalletData();
            return res;
        }

        [HttpPost]
        [Route("verifyHash")]
        public VerifyPostResponse verifyAmount(VerifyPostResponse hash)
        {
            VerifyPostResponse res = new VerifyPostResponse();
            res = BusinessRepository.checkWalletAmount(hash);
            return res;
        }

        [HttpGet]
        [Route("viewStatus")]
        public RequestStatusGetResponse viewRequestStatus()
        {
            RequestStatusGetResponse res = new RequestStatusGetResponse();
            res = BusinessRepository.FundingRequestStatus();
            return res;
        }
    }

}
