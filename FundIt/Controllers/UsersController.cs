﻿using FundIt.Models;
using FundIt.ProcessHelpers;
using FundIt.RepositoriesHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FundIt.Controllers
{
    public class UsersController : ApiController
    {
        [HttpPost]
        [Route("getData")]
        public SigupPostResponse saveData(Users user)
        {
            SigupPostResponse res = new SigupPostResponse();
            var responsebool = UserProcess.convertData(user);
            if (responsebool == true)
                res.message = "Registration Successful";
            else
                res.message = "Registration Unsuccessful";
            return res;
        }

    }
}
