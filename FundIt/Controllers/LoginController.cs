﻿using FundIt.Models;
using FundIt.ProcessHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;



namespace FundIt.Controllers
{
    
    public class LoginController : ApiController
    {
        [HttpPost]
        [Route("Login")]
        public LoginPostResponse loginTrial(Login login)
        {
            LoginPostResponse res = new LoginPostResponse();
            res = UserProcess.hashPassword(login);
            if (res.success == 1)
            {
                res.message = "login successfull";
                Temp_Data.temp_email = login.email;
            }
            else
            {
                res.message = "login unsuccessfull";
                
            }
            return res;
        }
    }
}
