﻿using FundIt.Models;
using FundIt.RepositoriesHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace FundIt.ProcessHelpers
{
    public class UserProcess
    {
        public static bool convertData(Users user)
        {
            //hashing password
            user.password =  HelperFunctions.sha256(user.password);
            
            
            //generating unique wallet_no
            Guid g = Guid.NewGuid();
            string guidString = Convert.ToBase64String(g.ToByteArray());
            guidString = guidString.Replace("=", "");
            guidString = guidString.Replace("+", "");
            user.wallet_no = guidString;
            return UsersRepository.addToUsers(user);
        }
        
        public static LoginPostResponse hashPassword(Login login)
        {
            login.password = HelperFunctions.sha256(login.password);
            return UsersRepository.checkUserLogin(login);
        }
    }
}