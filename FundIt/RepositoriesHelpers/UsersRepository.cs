﻿using FundIt.Connections;
using FundIt.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace FundIt.RepositoriesHelpers
{
    public class UsersRepository
    {
        static string funder;
        //Users user = new Users();
        //adding new users to database
        public static bool addToUsers(Users user)
        {
            var queryUser = "Insert into users(name,email,password,is_user_funder,wallet_no) values ('@name','@email','@password',@is_user_funder,'@wallet_no')";
            var queryWallet = "Insert into wallet(wallet_no,hash_code,amount) values ('@wallet_no','@hash_code',@amount)";

            if (user.is_user_funder == 1)
                funder = "1";
            else
                funder = "0";
                                            
            queryUser = queryUser.Replace("@name", user.name).Replace("@email", user.email).Replace("@password", user.password).Replace("@is_user_funder",funder).Replace("@wallet_no",user.wallet_no);
            queryWallet = queryWallet.Replace("@wallet_no", user.wallet_no).Replace("@hash_code", HelperFunctions.gen_coin(user)).Replace("@amount", "50");
            SqlConnection connection = new SqlConnection(DbConnector.connectionString);
          
            connection.Open();
            SqlCommand command1= new SqlCommand(queryUser, connection);
            SqlCommand command2 = new SqlCommand(queryWallet, connection);
            command1.ExecuteNonQuery();
            command2.ExecuteNonQuery();
            command1.Dispose();
            command2.Dispose();
            connection.Close();
            return true;
            
        }

        public static LoginPostResponse checkUserLogin(Login login)
        {
            var query = "Select is_user_funder,name from Users where email = '@email' and password = '@password'";
            LoginPostResponse res = new LoginPostResponse();
            string tempName="";
            res.is_user_funder = 0;
            query = query.Replace("@email", login.email).Replace("@password", login.password);
            SqlConnection connection = new SqlConnection(DbConnector.connectionString);
            connection.Open();
            using (var selectCommand = connection.CreateCommand())
            {
                selectCommand.CommandText = query;
                using (var reader = selectCommand.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (reader.Read())
                    {
                        tempName = (string)reader["name"];
                        res.is_user_funder = (int)reader["is_user_funder"];
                    }
                }
            }
            connection.Close();

            if (tempName == "")
            {
                res.success = 0;
                return res;
            }
            else
            {
                res.success = 1;
                return res;
            }    
        }
    }
    
}