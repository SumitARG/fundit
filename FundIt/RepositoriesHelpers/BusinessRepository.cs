﻿using FundIt.Connections;
using FundIt.Models;
using FundIt.ProcessHelpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace FundIt.RepositoriesHelpers
{
    public class BusinessRepository
    {
        //checking for ongoing requests
        public static bool checkRequestStatus(string email)
        {
            int status=0;
            var query = "Select top 1 is_ongoing from business where email = '@email' order by request_id desc";
            query = query.Replace("@email", email);
            SqlConnection connection = new SqlConnection(DbConnector.connectionString);
            connection.Open();
            using (var selectCommand = connection.CreateCommand())
            {
                selectCommand.CommandText = query;
                using (var reader = selectCommand.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (reader.Read())
                    {
                        status = (int)reader["is_ongoing"];
                    }
                }
            }
            connection.Close();

            if (status == 1)
                return false;
            else
                return true;
        }

        //Creating new Request in Business
        public static int createRequest(Activities activity)
        {
            int temp_request_id=0;
            string temp1 ="";
            var businessQuery = "Insert into business (email,product_name,is_ongoing) values ('@email','@product_name',1)";
            businessQuery = businessQuery.Replace("@email", Temp_Data.temp_email).Replace("@product_name", activity.product_name).Replace("@is_going", "1");
            var getReqIdQuery = "Select top 1 request_id from business where email = '@email' order by request_id desc";
            getReqIdQuery = getReqIdQuery.Replace("@email", Temp_Data.temp_email);
            var activityQuery = "Insert into activities (request_id,activity_name,duration,activity_cost,activity_on_going) values ('@request_id','@activity_name',@duration,'@activity_cost',0)";

            SqlConnection connection = new SqlConnection(DbConnector.connectionString);
            connection.Open();
            SqlCommand command1 = new SqlCommand(businessQuery, connection);
            command1.ExecuteNonQuery();
            command1.Dispose();
            using (var selectCommand = connection.CreateCommand())
            {
                selectCommand.CommandText = getReqIdQuery;
                using (var reader = selectCommand.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (reader.Read())
                    {
                        temp_request_id = (int)reader["request_id"];
                        temp1 = temp_request_id.ToString();
                    }
                }
            }
            connection.Close();
            connection.Open();
            var activityQuery1 = activityQuery.Replace("@request_id", temp1).Replace("@activity_name", activity.activity_name[0]).Replace("@duration", "1").Replace("@activity_cost",(activity.cost/5).ToString());
            SqlCommand command2 = new SqlCommand(activityQuery1,connection);
            command2.ExecuteNonQuery();
            command2.Dispose();
            connection.Close();
            connection.Open();
            var activityQuery2 = activityQuery.Replace("@request_id", temp1).Replace("@activity_name", activity.activity_name[1]).Replace("@duration", "1").Replace("@activity_cost", (activity.cost / 5).ToString());
            SqlCommand command3 = new SqlCommand(activityQuery2,connection);
            command3.ExecuteNonQuery();
            command3.Dispose();
            connection.Close();
            connection.Open();
            var activityQuery3 = activityQuery.Replace("@request_id", temp1).Replace("@activity_name", activity.activity_name[2]).Replace("@duration", "1").Replace("@activity_cost", (activity.cost / 5).ToString());
            SqlCommand command4 = new SqlCommand(activityQuery3, connection);
            command4.ExecuteNonQuery();
            command4.Dispose();
            connection.Close();
            connection.Open();
            var activityQuery4 = activityQuery.Replace("@request_id", temp1).Replace("@activity_name", activity.activity_name[3]).Replace("@duration", "1").Replace("@activity_cost", (activity.cost / 5).ToString());
            SqlCommand command5 = new SqlCommand(activityQuery4, connection);
            command5.ExecuteNonQuery();
            command5.Dispose();
            connection.Close();
            connection.Open();
            var activityQuery5 = activityQuery.Replace("@request_id", temp1).Replace("@activity_name", activity.activity_name[4]).Replace("@duration", "1").Replace("@activity_cost", (activity.cost / 5).ToString());
            SqlCommand command6 = new SqlCommand(activityQuery5, connection);
            command6.ExecuteNonQuery();
            command6.Dispose();
            connection.Close();

            return temp_request_id; 
        }

        public static void addToBusinessList(ListOfBusiness lb)
        {
            var query = "Insert into businesslist (request_id,product_name, cost, duration) values (@request_id,'@product_name',@cost,@duration)";
            query = query.Replace("@request_id", (lb.request_id).ToString()).Replace("@product_name", lb.product_name).Replace("@cost", (lb.cost).ToString()).Replace("@duration", (lb.duration).ToString());
            SqlConnection connection = new SqlConnection(DbConnector.connectionString);
            connection.Open();
            SqlCommand command1 = new SqlCommand(query, connection);
            command1.ExecuteNonQuery();
            command1.Dispose();
            connection.Close();
        }

        public static WalletPostResponse getWalletData()
        {
            string temp_no = "";
            string temp_hash = "";
            WalletPostResponse res = new WalletPostResponse();
            var query = "Select w.wallet_no, hash_code from wallet w inner join users u on w.wallet_no = u.wallet_no where u.email = '@email'";
            query = query.Replace("@email", Temp_Data.temp_email);

            SqlConnection connection = new SqlConnection(DbConnector.connectionString);
            connection.Open();
            using (var selectCommand = connection.CreateCommand())
            {
                selectCommand.CommandText = query;
                using (var reader = selectCommand.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (reader.Read())
                    {
                        temp_no = (string)reader["wallet_no"];
                        temp_hash = (string)reader["hash_code"];
                    }
                }
            }
            connection.Close();
            res.wallet_no = temp_no;
            res.hash_code = temp_hash;
            return res;
        }

        public static VerifyPostResponse checkWalletAmount(VerifyPostResponse hash)
        {
            //VerifyPostResponse res = new VerifyPostResponse();
            int temp_amount=0; 
            var query = "Select amount from wallet where hash_code = '@hash'";
            query = query.Replace("@hash", hash.hash_code);
            SqlConnection connection = new SqlConnection(DbConnector.connectionString);
            connection.Open();
            using (var selectCommand = connection.CreateCommand())
            {
                selectCommand.CommandText = query;
                using (var reader = selectCommand.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (reader.Read())
                    {
                        temp_amount = (int)reader["amount"];
                    }
                }
            }
            connection.Close();
            hash.amount = temp_amount;
            return hash;
        } 

        public static RequestStatusGetResponse FundingRequestStatus()
        {
            RequestStatusGetResponse res = new RequestStatusGetResponse();
            int temp_request_id = 0;
            var query = "Select request_id,product_name from business where is_ongoing = 1 and email = '@email'";
            query = query.Replace("@email", Temp_Data.temp_email);
            SqlConnection connection = new SqlConnection(DbConnector.connectionString);
            connection.Open();
            using (var selectCommand = connection.CreateCommand())
            {
                selectCommand.CommandText = query;
                using (var reader = selectCommand.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (reader.Read())
                    {
                        res.product_name = (string)reader["product_name"];
                      temp_request_id  = (int)reader["request_id"];
                    }
                }
            }
            connection.Close();
            query = "Select activity_name,activity_id from activities where request_id = '@request_id' and activity_on_going = 1";
            query = query.Replace("@request_id", temp_request_id.ToString());
            connection.Open();
            using (var selectCommand = connection.CreateCommand())
            {
                selectCommand.CommandText = query;
                using (var reader = selectCommand.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (reader.Read())
                    {
                        res.activity_name = (string)reader["activity_name"];
                    }
                }
            }
            connection.Close();
            return res;
        }
    }
}